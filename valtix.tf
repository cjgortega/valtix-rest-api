module "valtix-gateway" {
  source              = "git::ssh://git@bitbucket.org/flexdhdev/terraform-valtix-gateway.git//?ref=5.0.0"
  resource_suffix     = "waf"
  gateway_name        = "test-waf-gw"
  gcp_external_vpc    = data.google_compute_network.external_vpc.self_link
  gcp_shared_vpc      = data.google_compute_network.gcp_shared_vpc.self_link
  gateway_image       = "2.11-05"
  gcp_dmz_subnet      = "v2-infra-test-dmz-us-east1"
  gcp_external_subnet = "v2-infra-test-vatlix-mgmt-us-east1"
  gcp_project_name    = local.project
  availability_zones  = jsonencode(local.east-zone)
  gcp_region          = local.region
  security_type       = "INGRESS"
}

module "valtix-policy-rules" {
  depends_on         = [module.valtix-gateway]
  source             = "git::ssh://git@bitbucket.org/flexdhdev/terraform-valtix-ingress-policy-services.git//?ref=3.0.0"
  region             = local.region
  shared_vpc         = local.gcp_shared_vpc
  gateway_name       = "test-waf-gw"
  backend_address    = local.backend_address
  policy_rule_set_id = module.valtix-gateway.ingress_policy_rule_set_id
  event_suppressor = {
    "920340" = ["0.0.0.0/0"],
    "949110" = ["0.0.0.0/0"],
    "920130" = ["0.0.0.0/0"],
    "200002" = ["0.0.0.0/0"],
    "200005" = ["0.0.0.0/0"]
  }
  ips_event_suppressor = {
    "37784" = ["0.0.0.0/0"]
  }
}

resource "google_compute_firewall" "valtix_ingress_gateway" {
  name          = "temp-ssh-troubleshooting"
  network       = local.gcp_external_vpc
  source_ranges = [local.personal_ip]
  target_tags   = ["test-waf-gw-mgmt-${local.region}"]
  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
}
