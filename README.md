# Valtix-rest-api #

### What is this repository for? ###

This repository contains necessary work to perform troubleshooting on Valtix WAF, this is needed because our test gateways have a lot of traffic, which makes hard to troubleshoot and find details on logs.

### What is needed? ###

* Docker
* Terraform =>0.13.0
* Google credentails to GCP project
* Google Connection to brightinsight-artifacts

### How to run? ###

* If necessary make any changes on Python API, `make build/make push` to build and push to repository
* To apply Terraform make sure locals have the values where this will be deployed
* This needs to be executed in 2 steps since valtix-policy-rules uses a parameter that will be created only when execute valtix-gateway module.
* To execute in 2 steps first comment lines from 16 to 34 on valtix.tf, once valtix gateway gets createed execute terraform a second time uncommenting lines 16 to 34. 


### Trobleshooting WAF ###

* If your personal ip was placed in locals on main.tf, it was created a firwall rule allowing you to ssh into valtix instance
* Go to VM instances, add your public key to instance and use the external ip from external-vpc to ssh into vm.
* On /valtix it will have all configuration set to run the gateway.
* File /valtixnewer-fs/etc/nginx/modsec/modsecurity.conf holds the configuration for modsecurity, and most of the times is the initial point to troubleshoot WAF issues.
* Find entries below, uncomment it and change values to match.
`
SecDebugLog /var/log/modsec_debug.log
SecDebugLogLevel 9
`
* Execute `/valtix/scripts/restart-datapath.sh` to restart gateway.
* After 2 minutes Verify all processes are up `ps -ef | grep dp-worker`
* Verify log at `/var/log/modsec_debug.log`


#### DON'T FORGET TO DESTROY RESOURCES ONCE DONE ####