terraform {
  required_version = ">= 0.13.0"
  required_providers {
    valtix = {
      source = "valtix-security/valtix"
    }
  }
  backend "gcs" {
    bucket = "v2-infra-test-tfstate"
    prefix = "nonprod/test-vatix-waf"
  }
}

locals {
  project          = "v2-infra-test-vpc"
  product_project  = "v2-infra-test-product"
  region           = "us-east1"
  east-zone        = ["us-east1-b"]
  cluster_name     = "v2-infra-test-product-us-east-16"
  gcp_shared_vpc   = "v2-infra-test-shared-vpc"
  gcp_external_vpc = "v2-infra-test-external-vpc"
  k8s_api_name     = "test-rest-app"
  namespace        = "dev-cureall"
  hosts            = "api.us-east1.dev-cureall.brightinsight.com"
  host_internal    = "api-internal.us-east1.dev-cureall.brightinsight.com"
  uri_prefix       = "/test-rest-app/"
  personal_ip      = "188.214.143.23"
  rest_api_image   = "us.gcr.io/brightinsight-artifacts/rest-sample-python-api:0.1.0"
  backend_address = {
    "rest-api" = {
      "cert-name"    = "tls-dev-cureall",
      "sni"          = ["api.us-east1.dev-cureall.brightinsight.com"],
      "ip-list-fqdn" = ["api-internal.us-east1.dev-cureall.brightinsight.com"],
      "dest-back-ports" = {
        "HTTPS" = {
          "443" = "443"
        }
      },
    }
  }
}

provider "google" {
  project = local.project
  region  = local.region
  #version = "3.45"
}

provider "google" {
  alias   = "product"
  project = local.product_project
  region  = local.region
  #version = "3.45"
}

provider "valtix" {
  acctname     = "brightinsight"
  server       = "prod1-apiserver.vtxsecurityservices.com"
  port         = "8091"
  api_key_file = file("${path.module}/terraform-key.json")
}

data "google_compute_network" "gcp_shared_vpc" {
  name = local.gcp_shared_vpc
}

data "google_compute_network" "external_vpc" {
  name = local.gcp_external_vpc
}

data "google_client_config" "current" {
  provider = google.product
}

data "google_container_cluster" "gke" {
  provider = google.product
  project  = local.product_project
  name     = local.cluster_name
  location = local.region
}

provider "kubernetes" {
  version                = "~> 1.13.3"
  host                   = "https://${data.google_container_cluster.gke.endpoint}"
  token                  = data.google_client_config.current.access_token
  cluster_ca_certificate = base64decode(data.google_container_cluster.gke.master_auth[0].cluster_ca_certificate)
}

provider "helm" {
  version = "2.0.2"
  kubernetes {
    host                   = "https://${data.google_container_cluster.gke.endpoint}"
    token                  = data.google_client_config.current.access_token
    cluster_ca_certificate = base64decode(data.google_container_cluster.gke.master_auth[0].cluster_ca_certificate)
  }
}

