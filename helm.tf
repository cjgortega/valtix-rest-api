resource "helm_release" "prismacloud-istio-ingress" {
  name  = local.k8s_api_name
  chart = "${path.module}/helm"

  set {
    name  = "name"
    value = local.k8s_api_name
  }

  set {
    name  = "namespace"
    value = local.namespace
  }

  set {
    name  = "hosts"
    value = local.hosts
  }

  set {
    name  = "host_internal"
    value = local.host_internal
  }

  set {
    name  = "uri_prefix"
    value = local.uri_prefix
  }

}
