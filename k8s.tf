resource "kubernetes_deployment" "rest_api" {
  metadata {
    name      = local.k8s_api_name
    namespace = local.namespace
    labels = {
      app = local.k8s_api_name
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = local.k8s_api_name
      }
    }

    template {
      metadata {
        labels = {
          app = local.k8s_api_name
        }
      }

      spec {
        container {
          image             = local.rest_api_image
          name              = local.k8s_api_name
          image_pull_policy = "Always"

          port {
            container_port = 5000
            name           = "http"
            protocol       = "TCP"
          }

          resources {
            limits {
              cpu    = "0.5"
              memory = "200Mi"
            }
            requests {
              cpu    = "0.2"
              memory = "100Mi"
            }
          }

          security_context {
            allow_privilege_escalation = false
            privileged                 = false
            read_only_root_filesystem  = false
            run_as_group               = 0
            run_as_non_root            = false
            run_as_user                = 0
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "rest_api_service" {
  metadata {
    name      = local.k8s_api_name
    namespace = local.namespace
  }
  spec {
    selector = {
      app = kubernetes_deployment.rest_api.metadata.0.labels.app
    }

    port {
      port        = 8080
      target_port = 5000
    }

    type = "ClusterIP"
  }
}
